#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Tempus points of interest data importer

import provider

def import_poi_generic(args, shape_options):
    """Load a point shapefile into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A source name must be supplied. Use --source-name\n")
        sys.exit(1)
    subs['source_name'] = args.source_name[0]
    
    if args.prefix is None:
        args.prefix = ''
    
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
    
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
    
    if args.source_comment is None:
        subs["source_comment"] = ""
    else:
        subs["source_comment"] = args.source_comment
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.filter is None:
        subs["filter"] = "true"
    else:
        subs["filter"] = args.filter 
    
    if args.link:
        subs["link"] = "True"
    else:
        subs["link"] = "False"
    
    if args.max_dist:
        subs["max_dist"] = str(args.max_dist)
    else: 
        subs["max_dist"] = "50"
    
    subs["poi_type"] = args.poi_type
    
    if args.name_field is None:
        subs["name_field"] = "name"
    else:
        subs["name_field"] = args.name_field
    if args.id_field is None:
        subs["id_field"] = "id"
    else:
        subs["id_field"] = args.id_field
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    poii = provider.ImportPOIGeneric(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)
    return poii.run()
    
    
def import_poi_insee_bpe(args, shape_options):
    """Load INSEE BPE POI data into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A source name must be supplied. Use --source-name\n")
        sys.exit(1)
    subs['source_name'] = args.source_name[0]
    
    if args.prefix is None:
        args.prefix = ''
    
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
    
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
    
    if args.source_comment is None:
        subs["source_comment"] = ""
    else:
        subs["source_comment"] = args.source_comment
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.filter is None:
        subs["filter"] = "true"
    else:
        subs["filter"] = args.filter 
    
    if args.link:
        subs["link"] = "True"
    else:
        subs["link"] = "False"
        
    if args.max_dist:
        subs["max_dist"] = str(args.max_dist)
    else: 
        subs["max_dist"] = "50"
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    shape_options['S'] = False
    
    bpei = provider.ImportPOIINSEEBPE(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)
    return bpei.run()

    
def import_poi_ign_bdtopo(args, shape_options):
    """Load INSEE BPE POI data into a Tempus database."""
    subs={}
    if args.source_name is None:
        sys.stderr.write("A source name must be supplied. Use --source-name\n")
        sys.exit(1)
    subs['source_name'] = args.source_name[0]
    
    if args.prefix is None:
        args.prefix = ''
    
    if args.source_srid is None:
        subs["source_srid"] = "2154"
    else:
        subs["source_srid"] = args.source_srid
    
    if args.target_srid is None:
        subs["target_srid"] = "2154"
    else: 
        subs["target_srid"] = args.target_srid
    
    if args.source_comment is None:
        subs["source_comment"] = ""
    else:
        subs["source_comment"] = args.source_comment
    
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    if args.merge:
        subs["merge"] = "True"
    else:
        subs["merge"] = "False"
    
    if args.filter is None:
        subs["filter"] = "true"
    else:
        subs["filter"] = args.filter 
    
    if args.link:
        subs["link"] = "True"
    else:
        subs["link"] = "False"
    
    if args.max_dist:
        subs["max_dist"] = str(args.max_dist)
    else: 
        subs["max_dist"] = "50"
        
    if args.noclean:
        subs["no_clean"] = "True"
    else:
        subs["no_clean"] = "False"
    
    subs["temp_schema"]=provider.config.TEMPSCHEMA
    
    Importer = {
        '3': provider.ImportPOIIGNBDTopo_3,
        None: provider.ImportPOIIGNBDTopo_3
    }[args.model_version]
    bdtopoi = Importer(path=args.path[0], prefix=args.prefix, dbstring=args.dbstring, logfile=args.logfile, options=shape_options, subs=subs)
    return bdtopoi.run()

    