
CREATE TABLE %(temp_schema).pt_stop AS
SELECT node.id as node_id, 
       node.original_id, 
       pt_stop.name,
       pt_stop.security_time,
       pt_stop.traffic_rules, 
       pt_stop.gtfs_location_type, 
       pt_stop.pt_fare_zone_id,
       pt_stop.url,
       pt_stop.description,        
       node.geom
FROM tempus_networks.node JOIN tempus_networks.pt_stop ON (node.id = pt_stop.node_id)
WHERE node.source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_id)'); 


CREATE TABLE %(temp_schema).pt_station AS
SELECT poi_geom.id,
       poi_geom.name, 
       poi_geom.geom
FROM tempus_networks.poi_geom
WHERE source_id = (SELECT id FROM pgtempus.source WHERE name = '%(source_id)') AND type = 1; 


CREATE TABLE %(temp_schema).pt_fare_zone AS
SELECT DISTINCT pt_fare_zone.id, 
                pt_fare_zone.original_id, 
                pt_fare_zone.name, 
                pt_fare_zone.geom
FROM tempus_networks.pt_fare_zone JOIN tempus_networks.pt_stop ON (pt_stop.pt_fare_zone_id = pt_fare_zone.id)
                                  JOIN tempus_networks.node ON (node.id = pt_stop.node_id)
                                  JOIN pgtempus.source ON (source.id = arc.source_id)
WHERE source.name = '%(source_id)';


CREATE TABLE %(temp_schema).pt_section AS
SELECT pt_section.arc_id, 
       pt_section.sub_arcs, 
       arc.node_from_id as pt_stop_from, 
       arc.node_to_id as pt_stop_to, 
       arc.geom
FROM tempus_networks.pt_section JOIN tempus_networks.arc ON (pt_section.arc_id = arc.id)
                                JOIN pgtempus.source ON (source.id = arc.source_id)
WHERE source.name = '%(source_id)';


CREATE TABLE %(temp_schema).pt_agency AS
SELECT pt_agency.id, 
       pt_agency.original_id, 
       pt_agency.name, 
       pt_agency.url, 
       pt_agency.timezone, 
       pt_agency.lang
FROM tempus_networks.pt_agency JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)'; 


CREATE TABLE %(temp_schema).pt_route AS
SELECT pt_route.id, 
       pt_route.original_id, 
       pt_route.pt_agency_id, 
       pt_route.short_name, 
       pt_route.long_name, 
       pt_route.description, 
       pt_route.url, 
       pt_route.color, 
       pt_route.text_color
FROM tempus_networks.pt_route JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                              JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_trip AS
SELECT pt_trip.id, 
       pt_trip.original_id, 
       pt_trip.pt_route_id, 
       pt_trip.days_period_id, 
       pt_trip.traffic_rules, 
       pt_trip.pt_fare, 
       pt_trip.pt_trip_types_id, 
       pt_trip.name, 
       pt_trip.direction, 
       pt_trip.direction_headsign
FROM tempus_networks.pt_trip JOIN tempus_networks.pt_route ON (pt_trip.pt_route_id = pt_route.id)
                             JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                             JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)'; 


CREATE TABLE %(temp_schema).days_period AS
SELECT id, 
       original_id, 
       name, 
       days
FROM tempus_networks.days_period JOIN tempus_networks.pt_trip ON (pt_trip.days_period_id = days_period.id)
                                 JOIN tempus_networks.pt_route ON (pt_trip.pt_route_id = pt_route.id)
                                 JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                 JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_fare_rule AS
SELECT pt_fare_rule.id, 
       pt_fare_rule.pt_agency_id, 
       pt_fare_rule.currency_type, 
       pt_fare_rule.payment_method, 
       pt_fare_rule.max_transfers, 
       pt_fare_rule.max_transfer_duration, 
       pt_fare_rule.max_travel_duration, 
       pt_fare_rule.name, 
       pt_fare_rule.description 
FROM pgtempus.pt_fare_rule JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_fare_rule.pt_agency_id)
                           JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_zonal_fare AS
SELECT pt_zonal_fare.pt_fare_rule_id, 
       pt_zonal_fare.pt_route_id, 
       pt_zonal_fare.zone_id_from,
       pt_zonal_fare.zone_id_to,
       pt_zonal_fare.fare
FROM tempus_networks.pt_zonal_fare JOIN tempus_networks.pt_route ON (pt_zonal_fare.pt_route_id = pt_route.id)
                                   JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                   JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_unit_fare AS
SELECT pt_unit_fare.pt_fare_rule_id, 
       pt_unit_fare.pt_route_id, 
       pt_unit_fare.fare
FROM tempus_networks.pt_unit_fare JOIN tempus_networks.pt_route ON (pt_zonal_fare.pt_route_id = pt_route.id)
                                  JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                  JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_od_fare AS
SELECT pt_od_fare.pt_fare_rule_id, 
       pt_od_fare.pt_route_id, 
       pt_od_fare.pt_stop_id_from,
       pt_od_fare.pt_stop_id_to,
       pt_od_fare.fare
FROM tempus_networks.pt_od_fare JOIN tempus_networks.pt_route ON (pt_zonal_fare.pt_route_id = pt_route.id)
                                JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).pt_section_stop_times AS
SELECT arc_id, 
       pt_trip_id, 
       time_from,
       time_to, 
       interpolated_time_from,
       interpolated_time_to,
       direction_label,
       pickup_type,
       dropoff_type
FROM tempus_networks.pt_section_stop_times JOIN tempus_networks.pt_trip ON (pt_trip.id = pt_section_stop_times.pt_trip_id)
                                           JOIN tempus_networks.pt_route ON (pt_trip.pt_route_id = pt_route.id)
                                           JOIN tempus_networks.pt_agency ON (pt_agency.id = pt_route.pt_agency_id)
                                           JOIN pgtempus.source ON (source.id = pt_agency.source_id)
WHERE source.name = '%(source_name)';


CREATE TABLE %(temp_schema).arc AS
SELECT id, 
       original_id, 
       node_from_id,
       node_to_id,
       type,
       length,
       crossability,
       diffusion,
       geom
FROM tempus_networks.arc JOIN tempus_networks.node node_from ON (arc.node_from_id = node_from.id)
                         JOIN tempus_networks.node node_to ON (arc.node_to_id = node_to.id)
                         JOIN pgtempus.source ON (source.id = arc.source_id)                         
WHERE source.name = '%(source_name)' AND arc.source_id = node_from.source_id AND arc.source_id = node_to.source_id;


       

