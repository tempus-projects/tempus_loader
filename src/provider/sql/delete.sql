-- Tempus - data source delete Wrapper
-- Parameter %(source_name) : POI source name to delete


-- Delete
do 
$$
begin
    raise notice '==== Delete data source %(source_name) ===';
end
$$;

DELETE FROM pgtempus.source
WHERE name = '%(source_name)'; 

DELETE FROM tempus_networks.arc
WHERE (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') = ANY (arc.sources_id);

DELETE FROM tempus_networks.arcs_sequence
WHERE (SELECT id FROM pgtempus.source WHERE name = '%(source_name)') = ANY (arcs_sequence.sources_id);

REFRESH MATERIALIZED VIEW tempus_networks.arc_complete;
REFRESH MATERIALIZED VIEW tempus_networks.node_complete;
