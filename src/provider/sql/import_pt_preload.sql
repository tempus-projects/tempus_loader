/*
        Substitutions options
        %(source_name): name of the public transport network to create
        %(temp_schema): name of schema containing temporary data
*/

do $$
begin
raise notice '==== Remove constraints and indexes ===';
end$$;

SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arc');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.road_section_attributes');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arcs_sequence_time');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.arcs_sequence_toll');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_stop');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_agency');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_trip');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_route');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_fare_rule');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_unit_fare');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_od_fare');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_zonal_fare');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_fare_zone');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_section');
SELECT pgtempus.tempus_remove_foreign_keys_and_indexes('tempus_networks.pt_section_stop_times');

do $$
begin
raise notice '==== Remove triggers ===';
end$$;

SELECT pgtempus.tempus_remove_triggers();

do $$
begin
raise notice '==== Create new source if needed ===';
end$$;

do
$$
begin
    IF %(merge) = False THEN
        DELETE FROM pgtempus.source
        WHERE name = '%(source_name)';
        
        INSERT INTO pgtempus.source(id, name, type, comment)
        VALUES((SELECT coalesce(max(id)+1, 1) FROM pgtempus.source), '%(source_name)'::character varying, 2, '%(source_comment)'::character varying);
        
        ALTER SEQUENCE pgtempus.source_id_seq MINVALUE 0;
        PERFORM setval('pgtempus.source_id_seq', (SELECT coalesce(max(id), 0) FROM pgtempus.source));
    END IF;
end
$$;



