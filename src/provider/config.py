#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2018-2019 Cerema (https://www.cerema.fr)
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

import sys
import os
import string
import subprocess
from enum import IntEnum

PYTHON="python3"
TEMPUSLOADER=(os.path.dirname(__file__)+"/lib/tempus_loader/src/tempus_loader.py" if sys.platform.startswith('win') else "tempus_loader")
CREATEDB=("C:/OSGeo4W64/bin/createdb.exe" if sys.platform.startswith('win') else "createdb")
DROPDB=("C:/OSGeo4W64/bin/dropdb.exe" if sys.platform.startswith('win') else "dropdb")
PGRESTORE=("C:/OSGeo4W64/bin/pg_restore.exe" if sys.platform.startswith('win') else "pg_restore")
PGDUMP=("C:/OSGeo4W64/bin/pg_dump.exe" if sys.platform.startswith('win') else "pg_dump")
PSQL=("C:/OSGeo4W64/bin/psql.exe" if sys.platform.startswith('win') else "psql")
SHP2PGSQL=("C:/OSGeo4W64/bin/shp2pgsql.exe" if sys.platform.startswith('win') else "shp2pgsql")
PGSQL2SHP=("C:/OSGeo4W64/bin/pgsql2shp.exe" if sys.platform.startswith('win') else "pgsql2shp")
TEMPSCHEMA = "_temp"

TABLE_PT_STOP_POINT="arret_tc_point_arret"
TABLE_PT_STOP_AREA="arret_tc_zone_arret"
TABLE_PT_ROUTE_TRIP="ligne_tc_service"
TABLE_PT_ROUTE_PATH="ligne_tc_itineraire"
TABLE_PT_ROUTE="ligne_tc"
TABLE_PT_AGENCY="exploitant_tc"
TABLE_OD_ARC="od_arc"
TABLE_OD_TRIP="od_trajet"
TABLE_OD_PATH="od_chemin"
TABLE_OD="od"
TABLE_O_OR_D_ISOCHRON="isochrone"

# Traffic rules 
# (must be conform to the database table pgtempus.traffic_rule);
class TrafficRules(IntEnum):
    WALKING = 1
    WHEELCHAIR = 2
    BICYCLE = 3
    CAR_WITH_TOLL = 4
    CAR_WITHOUT_TOLL = 5
    TAXI = 6
    CARPOOLING = 7
    TRUCK = 8
    COACH = 9

# Speed rules 
# (must be conform to the database table pgtempus.speed_rule);
class SpeedRules(IntEnum):
    CAR = 1

# Toll rules 
# (must be conform to the database table pgtempus.toll_rule);
class TollRules(IntEnum):
    CLASS1 = 1
    CLASS2 = 2
    CLASS3 = 3
    CLASS4 = 4
    CLASS5 = 5

# Vehicle parking rules
# (must be conform to the database table pgtempus.vehicle_parking_rule);
class VehicleParkingRules(IntEnum):
    PRIVATE_BICYCLE = 1
    PRIVATE_CAR = 2

# PT fares
# (must be conform to the database pgtempus.pt_fare_category type);
class PTFares(IntEnum):
    ZONAL = 1
    UNIT = 2
    OD = 3
    
class Agregates(IntEnum):
    AVG = 1
    MIN = 2
    MAX = 3
