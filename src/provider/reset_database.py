#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""


from provider.data_dir_manager import DataDirManager
from provider.data_zip_manager import DataZipManager

# Module to reset the tempus schema
class ResetTempus(DataZipManager):
    """This class allows to reset the indic and tempus_parameters schemas of a database"""
    PRE_SQL = [ 
                'reset_tempus_schemas.sql', 
                'reset_preload.sql'
              ]
    IMPORT_CSVTXTFILES = []
    POST_SQL = [ 'reset_postload.sql' ]
    
    
class ResetTempusAccess(DataZipManager):
    """This class allows to reset the indic and tempus_parameters schemas of a database"""
    PRE_SQL = [ 
                'reset_tempus_schemas.sql', 
                'reset_tempusaccess_function_pt_stop.sql', 
                'reset_tempusaccess_function_pt_route.sql', 
                'reset_tempusaccess_function_pt_agency.sql', 
                'reset_tempusaccess_function_od.sql', 
                'reset_tempusaccess_function_o_or_d.sql', 
                'reset_preload.sql' 
              ]
    IMPORT_CSVTXTFILES = [ ('modality', True), 
                           ('agregate', True), 
                           ('data_format', True), 
                           ('holiday_period', True), 
                           ('indic', True), 
                           ('base_obj_type', True)
                         ]
    POST_SQL = [ 'reset_postload.sql' ]
    
    
