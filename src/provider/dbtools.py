#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
/**
 *   Copyright (C) 2012-2013 IFSTTAR (http://www.ifsttar.fr)
 *   Copyright (C) 2012-2013 Oslandia <infos@oslandia.com>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Library General Public
 *   License as published by the Free Software Foundation; either
 *   version 2 of the License, or (at your option) any later version.
 *   
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Library General Public License for more details.
 *   You should have received a copy of the GNU Library General Public
 *   License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */
"""

#
# Tempus data loader

import os
import sys
import subprocess
import tempfile
from provider.config import *

def extract_dbparams(dbstring):
    """Get a dictionnary out of a classic dbstring."""
    ret = {}
    if dbstring:
        ret = dict([(i, j.strip("' ")) for i, j in [i.split('=') for i in dbstring.split(' ')]])
    return ret

class PsqlLoader:
    def __init__(self, dbstring = "", sqlfile = "", filename = "", replacements = {}, logfile = None):
        """Psql Loader constructor

        dbstring is a connexion string to a PostGIS database
            Ex : "dbname='databasename' host='addr' port='5432' user='x'"

        sqlfile is a text file containing SQL instructions
        
        replacements is a dictionnary containing values for key found in sqlfile to
            be replaced. Every occurence of "%key%" in the file will be replaced
            by the corresponding value in the dictionnary.
        """
        self.dbparams = extract_dbparams(dbstring) 
        self.sqlfilecontents = sqlfile
        self.filename = ''
        self.replacements = replacements
        self.logfile = logfile

    def set_sqlfile(self, sqlfile):
        self.filename = sqlfile

    def set_from_template(self, template_filename, values):
        """Set the SQL file by filling a template with values."""
        with open(template_filename, "r", encoding="utf-8") as fi:
            with tempfile.NamedTemporaryFile(mode="w", encoding="utf-8", delete=False) as tmpfile:
                self.filename = tmpfile.name
                # replace every occurence of "%(key)" with its corresponding value
                t = fi.read()
                for k, v in values.items():
                    t = t.replace( '%(' + k + ')', v )
                tmpfile.write(t)
        
    def set_dbparams(self, dbstring):
        """Set database parameters."""
        self.dbparams = extract_dbparams(dbstring)

    def load(self):
        """Load SQL file into the DB."""
        res = False

        # call psql with sqlfile
        command = [PSQL]
        if 'host' in self.dbparams:
            command.append("--host=%s" % self.dbparams['host'])
        if 'user' in self.dbparams:
            command.append("--username=%s" % self.dbparams['user'])
        if 'port' in self.dbparams:
            command.append("--port=%s" % self.dbparams['port'])
        if 'dbname' in self.dbparams:
            command.append("--dbname=%s" % self.dbparams['dbname'])
        if self.logfile:
            try:
                out = open(self.logfile, "a")
                err = out
            except IOError as err:
                sys.stderr.write("%s : I/O error : %s\n" % (self.logfile, err))
        else:
            out = sys.stdout
            err = sys.stderr
        retcode = 0
        try:
            out.write("======= Executing SQL %s\n" % os.path.basename(self.filename) )
            out.flush()
            p = subprocess.Popen(command + ["-f", self.filename, "-v", "ON_ERROR_STOP=1"], stdin = subprocess.PIPE, stdout = out, stderr = err)
            p.communicate()
            out.write("\n")
            retcode = p.returncode
        except OSError as err:
            sys.stderr.write("Error calling %s : %s \n" % (" ".join(command), err))
        if self.logfile:
            out.close()
        return retcode == 0


